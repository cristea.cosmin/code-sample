import React, { useState, createContext } from 'react'

export const UiContext = createContext();
export const UiProvider = props => {
    const [ui, setUi] = useState({
        navbar: ['nav 1', 'nav 2', 'nav 3'],
        sidebar: [...Array(12).keys()].map(el => `option-${(el + 1)}`),
        selectedNavbar: ['nav 1'],
        selectedSidebar: ['option-1']
    })
    
    return (<UiContext.Provider value={[ui, setUi]}>{props.children}</UiContext.Provider>)
}