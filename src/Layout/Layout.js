import React from 'react'
import { Layout as AntdLayout } from 'antd';
import Header from './Header';
import Footer from './Footer';
import Content from './Content';
import { UiProvider } from './../Context/UiContext';
export default function Layout() {
    return (
        <div>
            <UiProvider>
                <AntdLayout>
                    <Header />
                    <Content />
                    <Footer />
                </AntdLayout>
            </UiProvider>
        </div>
    )
}
