import React, { useContext } from 'react'
import { Layout as AntdLayout, Menu } from 'antd';
import { UiContext } from './../Context/UiContext';

const { Header } = AntdLayout;

export default function MyHeader() {
    const [ui, setUi] = useContext(UiContext);

    const updateUi = ({ item, key, keyPath, selectedKeys, domEvent }) => {
        setUi(prevUi => { return { ...prevUi, selectedNavbar: [key] } });
    }

    return (
        <Header className="header">
            <Menu theme="dark"
                mode="horizontal"
                defaultSelectedKeys={ui.selectedNavbar}
                onSelect={updateUi}>
                {ui.navbar.map(el => (<Menu.Item key={el} >{el}</Menu.Item>))}
            </Menu>
        </Header>
    )
}
