import React, { useContext } from 'react'
import { Layout as AntdLayout, Menu, Form, Input, Button, Divider, Select } from 'antd';
import { UiContext } from './../Context/UiContext';
const { Content, Sider } = AntdLayout;

export default function MyContent() {
    const [ui, setUi] = useContext(UiContext);

    const updateUi = ({ item, key, keyPath, selectedKeys, domEvent }) => {
        setUi(prevUi => { return { ...prevUi, selectedSidebar: [key] } });
    }

    const addNav = (values) => {
        let defaults = {
            position: values?.navItemPosition ? values.navItemPosition : 'top',
            name: values?.navItem ? values.navItem : `nav ${ui.navbar.length + 1}`
        }

        switch (defaults.position) {
            case 'top':
                setUi(prevUi => {
                    // sanity check? 
                    if (prevUi.navbar.includes(defaults.name)) {
                        console.warn('Blocked')
                        return prevUi
                    }
                    return { ...prevUi, navbar: [...prevUi.navbar, defaults.name] }
                });
                break;
            case 'sidebar':
                setUi(prevUi => {
                    if (prevUi.sidebar.includes(defaults.name)) {
                        console.warn('Blocked')
                        return prevUi
                    }
                    return { ...prevUi, sidebar: [...prevUi.sidebar, defaults.name] }
                });
                break;
        }
    }
    return (
        <Content style={{ padding: '0 50px' }}>
            <AntdLayout className="site-layout-background" style={{ padding: '24px 0' }}>
                <Sider className="site-layout-background" width={200}>
                    <Menu
                        mode="inline"
                        defaultSelectedKeys={ui.selecteSidebar}
                        style={{ height: '100%' }}
                        onSelect={updateUi}
                    >
                        {ui.sidebar.map(el => <Menu.Item key={el}>{el}</Menu.Item>)}
                    </Menu>
                </Sider>
                <Content className="site-layout-background" style={{ background: '#fff', padding: '12px 24px', minHeight: 280 }}>
                    <h1>
                        State management using react hooks sample code
                    </h1>
                    <p>
                        Although it's a simple example, I thought it's a good sample to provide for review.
                    </p>

                    <p>Selected navbar: <em>{ui.selectedNavbar[0]}</em></p>
                    <p>Selected sidebar: <em>{ui.selectedSidebar[0]}</em></p>

                    <Divider />

                    <Form name={'simple-form'}
                        labelCol={{ span: 4 }}
                        wrapperCol={{ span: 14 }}
                        onFinish={addNav}
                    >
                        <Form.Item label={'New nav item'} name={'navItem'}>
                            <Input />
                        </Form.Item>
                        <Form.Item label={'Position'} name={'navItemPosition'}>
                            <Select
                                placeholder={'Select a nav position'}
                            >
                                <Select.Option value="top">Top</Select.Option>
                                <Select.Option value="sidebar">Sidebar</Select.Option>
                            </Select>
                        </Form.Item>
                        <Form.Item wrapperCol={{ span: 14, offset: 4 }}>
                            <Button type="primary" htmlType="submit" >
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>

                </Content>
            </AntdLayout>
        </Content >
    )
}

