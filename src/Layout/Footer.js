import React, { useContext } from 'react'
import { Layout as AntdLayout } from 'antd';
import { UiContext } from './../Context/UiContext';
const { Footer } = AntdLayout;

export default function MyFooter() {
    const [ui, setUi] = useContext(UiContext);
    return (
        <Footer style={{ textAlign: 'center' }}>
            <p>
                We do have access to UI here as well:
                navbar length is: {ui.navbar.length} <br />
                sidebar length is: {ui.sidebar.length}
            </p>
        </Footer>
    )
}
